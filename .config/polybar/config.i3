;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================
[colors]

alert = #bd2c40
xcolor0 = ${xrdb:color0}
xcolor1 = ${xrdb:color1}
xcolor2 = ${xrdb:color2}
xcolor3 = ${xrdb:color3}
xcolor4 = ${xrdb:color4}
xcolor5 = ${xrdb:color5}
xcolor6 = ${xrdb:color6}
xcolor7 = ${xrdb:color7}
xcolor8 = ${xrdb:color8}
xcolor9 = ${xrdb:color9}
xcolor10 = ${xrdb:color10}
xcolor11 = ${xrdb:color11}
xcolor12 = ${xrdb:color12}
xcolor13 = ${xrdb:color13}
xcolor14 = ${xrdb:color14}
xcolor15 = ${xrdb:color15}
background = ${xrdb:background}
foreground = ${xrdb:foreground}
;foreground = #888888
;foreground-alt = #555555

primary = ${colors.xcolor7}
secondary = ${colors.xcolor0}

[bar/i3bar]
;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 40
offset-x = 0%
offset-y = 0%
radius = 0
bottom = true
fixed-center = true

background = ${colors.xcolor0}
foreground = ${colors.xcolor7}

underline-size = 2
underline-color = ${colors.xcolor7}

line-size = 0
line-color = ${colors.xcolor7}

border-left-size = 0
border-color = #00000000
border-top-size = 0

padding-left = 0
padding-right = 3

module-margin-left = 3
module-margin-right = 3

; font-0 = sq:pixelsize=12;1:antialias=false:autohint=true
;font-0 = CtrlD:pixelsize=12;1:antialias=false
font-0 = Dina:pixelsize=11;1:antialias=false
font-1 = unifont:fontformat=truetype:size=9:antialias=false;2
font-2 = Wuncon Siji:pixelsize=11;1:antialias=false
;font-3 = Font Awesome 5 Free:pixelsize=11:antialias=false
;font-4 = Font Awesome:pixelsize=11:antialias=true

modules-left = i3 xwindow
modules-center = date
modules-right = battery temperature volume thememenu

tray-position = right
tray-padding = 3
tray-transparent = false
tray-scale = 1
tray-maxsize = 16
tray-background = ${colors.xcolor0}

;wm-restack = bspwm
wm-restack = i3

override-redirect = false

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

scroll-up = i3wm-wsprev
scroll-down = i3wm-wsnext

[bar/bspwmbar1]
;monitor = ${env:MONITOR:HDMI-1}
width = 30%
height = 40
offset-x = 44%
offset-y = 1%
radius = 0
bottom = true
fixed-center = true

background = ${colors.xcolor0}
foreground = ${colors.xcolor7}

underline-size = 0
underline-color = ${colors.xcolor7}

line-size = 0
line-color = ${colors.xcolor7}

border-left-size = 0
border-color = #00000000
border-top-size = 0

padding-left = 6
padding-right = 6

module-margin-left = 3
module-margin-right = 3

; font-0 = sq:pixelsize=12;1:antialias=false:autohint=true
;font-0 = CtrlD:pixelsize=12;1:antialias=false
font-0 = Dina:pixelsize=11;1:antialias=false
font-1 = unifont:fontformat=truetype:size=9:antialias=false;2
font-2 = Wuncon Siji:pixelsize=11;1:antialias=false
;font-3 = Font Awesome 5 Free:pixelsize=11:antialias=false
;font-4 = Font Awesome:pixelsize=11:antialias=true

modules-left = bspwm
modules-center = date
modules-right = battery

tray-position = right
tray-padding = 3
tray-transparent = false
tray-scale = 1
tray-maxsize = 16
tray-background = ${colors.xcolor0}

wm-restack = bspwm
;wm-restack = i3

override-redirect = false

scroll-up = bspwm-deskprev
scroll-down = bspwm-desknext

;scroll-up = i3wm-wsprev
;scroll-down = i3wm-wsnext

[module/xwindow]
type = internal/xwindow
#label = %title:...%
label = %title%
label-maxlen = 70

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.xcolor7}
format-prefix-underline = ${colors.xcolor7}

label-layout = %layout%
label-layout-underline = ${colors.xcolor7}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.xcolor7}
label-indicator-underline = ${colors.xcolor7}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.xcolor7}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.xcolor1}
label-focused-underline= ${colors.xcolor7}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.xcolor7}
label-empty-padding = 2

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 1
label-mode-foreground = ${colors.xcolor7}
label-mode-background = ${colors.xcolor0}

; focused = Active workspace on focused monitor
label-focused = %index% %icon%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index% %icon%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}


; visible = Active workspace on unfocused monitor
label-visible = %index% %icon%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index% %icon%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #ff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.xcolor7}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.xcolor7}
format-underline = #f90000
label = %percentage%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.xcolor7}
format-underline = #4bffdc
label = %percentage_used%%

[module/wlan]
type = internal/network
interface = net1
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #9f78e1
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.xcolor7}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.xcolor7}

[module/eth]
type = internal/network
interface = enp3s0
interval = 10.0

format-connected-underline = ${colors.xcolor8}
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.xcolor7}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.xcolor7}

[module/date]
type = internal/date
interval = 10

date =
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.xcolor7}
format-underline = 

label = %date% %time%

[module/volume]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = 
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.xcolor7}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.xcolor7}
bar-volume-foreground-1 = ${colors.xcolor7}
bar-volume-foreground-2 = ${colors.xcolor7}
bar-volume-foreground-3 = ${colors.xcolor7}
bar-volume-foreground-4 = ${colors.xcolor7}
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.xcolor7}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1
full-at = 97

format-charging = <animation-charging> <label-charging>
format-charging-underline = 

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.xcolor7}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.xcolor7}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.xcolor7}
animation-charging-framerate = 650

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 75

format = <ramp> <label>
format-underline = 
format-warn = <ramp> <label-warn>
format-warn-underline = 

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground =

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.xcolor7}

[module/powermenu]
type = custom/menu
 
expand-right = true
 
format-spacing = 1
 
label-open = shutdown
;label-open-foreground = ${colors.xcolor7}
label-open-underline = 
label-close = cancel
label-close-foreground = ${colors.xcolor7}
label-separator = |
label-separator-foreground = ${colors.xcolor7}
 
menu-0-0 = reboot
menu-0-0-exec = reboot
menu-0-1 = poweroff
menu-0-1-exec = shutdown now
menu-0-2 = suspend
menu-0-2-exec = pm-suspend

[module/thememenu]
type = custom/menu
 
expand-right = true
 
format-spacing = 1
 
label-open = theme
;label-open-foreground = ${colors.xcolor7}
label-open-underline = 
label-close = cancel
label-close-foreground = ${colors.xcolor7}
label-separator = |
label-separator-foreground = ${colors.xcolor7}
 
menu-0-0 = gruvbox
menu-0-0-exec = feh --bg-scale ~/Pictures/wallsnieuw/gruvbox.png && wal --theme base16-gruvbox-hard -g -o wal_steam
menu-0-1 = solarized
menu-0-1-exec = feh --bg-scale ~/Pictures/wallsnieuw/wallhaven-695106.jpg && wal --theme solarized -g -o wal_steam
menu-0-2 = google
menu-0-2-exec = feh --bg-scale ~/Pictures/wallsnieuw/google.png && wal --theme base16-google -g -o wal_steam
menu-0-3 = ocean
menu-0-3-exec = feh --bg-scale ~/Pictures/wallsnieuw/wallhaven-449801.jpg && wal --theme base16-ocean -g -o wal_steam
menu-0-4 = spacemacs
menu-0-4-exec = feh --bg-scale ~/Pictures/wallsnieuw/wallhaven-449801.jpg && wal --theme base16-spacemacs -g -o wal_steam

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
