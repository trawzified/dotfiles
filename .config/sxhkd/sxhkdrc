#!/bin/sh

#
# wm independent hotkeys
#

# terminal emulator
super + Return
	st	
# draw terminal emulator (credit to addy-fe)
super + shift + Return
	$HOME/Scripts/urdraw.sh

# program launcher
super + d
	rofi -show drun -theme ~/.cache/wal/colors-rofi-dark.rasi -font "Source Sans Pro 9"

# start firefox
super + w
	firefox

# start ranger
super + r
	st -e ranger 
# make sxhkd reload its configuration files:
super + alt + shift + r
	pkill -USR1 -x sxhkd

# set padding to 0
# super + {_,shift +}z
#	{bspc config window_gap 0 && bspc config top_padding 0 && bspc config bottom_padding 0 && bspc config left_padding 0 && bspc config right_padding 0 && bspc config border_width 0

#
# bspwm hotkeys
#

# quit bspwm normally
super + shift + Escape
	bspc quit

# close and kill
super + q
	bspc node -c

# start music player
super + m
	bspc rule -r St && bspc rule -a St -o state=tiling desktop='^4' follow=on focus=on && st -e ncmpcpp && bspc rule -a St desktop='^1' follow=on focus=on

# if the current node is automatic, send it to the last manual, otherwise pull the last leaf
super + y
	bspc query -N -n focused.automatic && bspc node -n last.!automatic || bspc node last.leaf -n focused

# disable compositor & second screen (game mode on)
super + g
	killall picom; killall polybar; $HOME/.config/polybar/launch.sh

# enable compositor & second screen (game mode off)
super + shift + g
	picom -b; killall polybar; $HOME/.config/polybar/launch.sh

#
# state/flags
#

# toggle floating

super + {_,shift +}space
	bspc node -t '~floating'

# toggle fullscreen
super + f
	bspc node -t '~fullscreen'

# toggle tiled
super + t
	bspc node -t '~tiled'


# set the node flags
super + ctrl + {x,y,z}
	bspc node -g {locked,sticky,private}


#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + shift + {y,u,i,o}
	bspc node -z {right -20 0, bottom 0 -20,bottom 0 20,right 20 0}

#super + alt + {h,j,k,l}
#	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
#super + alt + shift + {h,j,k,l}
#	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + shift + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# lock the screen
super + Escape
	i3lock-fancy -t ""

# easy access to PulseAudio volume control
super + a
	pavucontrol

# reload
super + shift + r
    ~/.config/bspwm/bspwmrc

# take a screenshot (fullscreen)
super + Print
	xfce4-screenshooter -f

# take a screenshot
Print
   xfce4-screenshooter

super + shift + x
	shutdown now

# Brightness up
#XF86MonBrightnessUp
#	xbacklight -inc 5


# Brightness down
#XF86MonBrightnessDown
#	xbacklight -dec 5

# Play/Pause
XF86AudioPlay
	mpc toggle

super + shift + w
	mpc toggle

# Next
XF86AudioNext
	mpc next
	#playerctl next

super + shift + e
	mpc next

# Previous
XF86AudioPrev
	mpc prev
	#playerctl previous

super + shift + q
	mpc prev

# Volume up
XF86AudioRaiseVolume
	amixer -D pulse sset Master 10%+ && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga

# Volume down
XF86AudioLowerVolume
	amixer -D pulse sset Master 10%- && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga

# Mute
XF86AudioMute
	amixer -D pulse sset Master toggle-mute
