set $term st
set $mod Mod4

# font
font xft:Source Sans Pro 11

# thin borders
hide_edge_borders both
gaps inner 12
# smart_gaps off
gaps outer 12


# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
# bindsym $mod+Left focus left
# bindsym $mod+Down focus down
# bindsym $mod+Up focus up
# bindsym $mod+Right focus right
 
# move focused window
bindsym $mod+Shift+h move left 35
bindsym $mod+Shift+j move down 35
bindsym $mod+Shift+k move up 35
bindsym $mod+Shift+l move right 35
 
# alternatively, you can use the cursor keys:
bindsym $mod+Left move left 35
bindsym $mod+Down move down 35
bindsym $mod+Up move up 35
bindsym $mod+Right move right 35
 
# resize windows
bindsym $mod+Shift+y resize shrink width 15 px or 4 ppt
bindsym $mod+Shift+u resize shrink height 15 px or 4 ppt
bindsym $mod+Shift+i resize grow height 15 px or 4 ppt
bindsym $mod+Shift+o resize grow width 15 px or 4 ppt

# drag windows
floating_modifier $mod

# adjust i3-gaps
bindsym $mod+z gaps inner current plus 3
bindsym $mod+Shift+z gaps inner current minus 3
bindsym $mod+Shift+d gaps inner current set 0; gaps outer current set 12

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen
bindsym $mod+F11 fullscreen

# change container layout
#bindsym $mod+s layout stacking
#bindsym $mod+w layout tabbed
#bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

set $workspace1 "I"
set $workspace2 "II"
set $workspace3 "III"
set $workspace4 "IV"
set $workspace5 "V"
set $workspace6 "VI"
set $workspace7 "VII"
set $workspace8 "VIII"
set $workspace9 "IX"
set $workspace10 "X"

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6
bindsym $mod+Shift+7 move container to workspace $workspace7
bindsym $mod+Shift+8 move container to workspace $workspace8
bindsym $mod+Shift+9 move container to workspace $workspace9
bindsym $mod+Shift+0 move container to workspace $workspace10

# switch to workspace
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6
bindsym $mod+7 workspace $workspace7
bindsym $mod+8 workspace $workspace8
bindsym $mod+9 workspace $workspace9
bindsym $mod+0 workspace $workspace10

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace
bindsym $mod+Shift+r restart

# exit i3
bindsym $mod+Shift+Escape exit

# easy shutdown
bindsym $mod+Shift+x exec shutdown now

########## Styling ##########

# Set colors from Xresources
# Change 'color7' and 'color2' to whatever colors you want i3 to use 
# from the generated scheme.
# NOTE: The '#f0f0f0' in the lines below is the color i3 will use if
# it fails to get colors from Xresources.

set_from_resource $fg i3wm.foreground #f0f0f0
set_from_resource $bg i3wm.background #f0f0f0
set_from_resource $xg i3wm.color1 #f0f0f0
set_from_resource $ag i3wm.color4 #f0f0f0

# class                 border  backgr. text indicator child_border
client.focused          $ag     $ag     $fg  $ag       $ag
client.focused_inactive $xg     $xg     $fg  $xg       $xg
client.unfocused        $bg     $xg     $fg  $xg       $xg
client.urgent           $bg     $bg     $fg  $bg       $bg
client.placeholder      $bg     $bg     $fg  $bg       $bg

client.background       $bg

########## i3-gaps integration ##########

for_window [class="^.*"] border pixel 6

for_window [class="Titelscherm"] floating enable

for_window [window_role="pop-up"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [title="Preferences$"] floating enable
for_window [title="Nitrogen"] floating enable
for_window [class="Pavucontrol"] floating enable
for_window [class="Skype"] floating enable
for_window [class="VLC*"] floating enable
for_window [class="mpv"] floating enable
for_window [class="URxvt" instance="float"] floating enable
for_window [class="St" instance="float"] floating enable

for_window [class="Atom"] move workspace $workspace1
for_window [class="St" instance="Normal"] move workspace $workspace1
for_window [class="Firefox"] move workspace $workspace2
for_window [class="qutebrowser"] move workspace $workspace2
for_window [class="Thunar"] move workspace $workspace2
for_window [class="Nautilus"] move workspace $workspace2
for_window [class="Spotify"] move workspace $workspace4
for_window [class="*"] move workspace $workspace4
for_window [class="Discord*"] move workspace $workspace2
for_window [class="Steam"] move workspace $workspace5
for_window [class="Syncthing*"] move workspace $workspace6
for_window [class="Bluetooth*"] move workspace $workspace6
for_window [class="Pavucontrol"] move workspace $workspace6

# Steam windows
for_window [class="^Steam$" title="^Friends$"] floating enable
for_window [class="^Steam$" title="Steam - News"] floating enable
for_window [class="^Steam$" title=".* - Chat"] floating enable
for_window [class="^Steam$" title="^Settings$"] floating enable
for_window [class="^Steam$" title=".* - event started"] floating enable
for_window [class="^Steam$" title=".* CD key"] floating enable
for_window [class="^Steam$" title="^Steam - Self Updater$"] floating enable
for_window [class="^Steam$" title="^Screenshot Uploader$"] floating enable
for_window [class="^Steam$" title="^Steam Guard - Computer Authorization Required$"] floating enable
for_window [title="^Steam Keyboard$"] floating enable

# Game Mode
bindsym $mod+g exec killall compton 
bindsym $mod+Shift+g exec compton -b

# Default Bindings

bindsym XF86MonBrightnessUp exec xbacklight -inc 10
bindsym XF86MonBrightnessDown exec xbacklight -dec 10
# bindsym XF86MonBrightnessUp exec light -A 10 # Increase Brightness
# bindsym XF86MonBrightnessDown exec light -U 10 # Decrease Brightness
# bindsym XF86MonBrightnessDown exec $HOME/.config/i3/brightness.sh - # Decrease Brightness
# bindsym XF86MonBrightnessUp exec $HOME/.config/i3/brightness.sh + # Increase Brightness
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -D pulse sset Master 10%+ && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -D pulse sset Master 10%- && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
bindsym XF86AudioMute exec --no-startup-id amixer -D pulse sset Master toggle-mute
bindsym XF86AudioPlay exec mpc toggle
bindsym XF86AudioPause exec mpc toggle
bindsym XF86AudioPrev exec mpc prev
bindsym XF86AudioNext exec mpc next

#For PULSEAUDIO/PAMIXER
set $inc --no-startup-id pamixer --allow-boost -i 5
set $biginc --no-startup-id pamixer --allow-boost -i 15
set $dec --no-startup-id pamixer --allow-boost -d 5
set $bigdec --no-startup-id pamixer --allow-boost -d 15
set $mute --no-startup-id pamixer --allow-boost -t
set $micmute --no-startup-id pamixer --allow-boost -t
set $truemute --no-startup-id pamixer -m

# Changed / additional bindings
bindsym $mod+Escape exec i3lock-fancy -t ""
bindsym $mod+q kill
bindsym $mod+w exec firefox
bindsym $mod+d exec rofi -show drun 
bindsym $mod+s exec rofi -show window
bindsym $mod+Return exec $term
bindsym $mod+Shift+Return exec ~/Scripts/urdraw.sh
bindsym $mod+r exec $term -e ranger
bindsym $mod+m exec $term -e ncmpcpp
bindsym $mod+\ exec --no-startup-id steam -fulldesktopres
bindsym $mod+a exec pavucontrol
bindsym $mod+n exec nautilus

# Screenshots (credit to airblader)
bindsym Print exec --no-startup-id "xfce4-screenshooter"
bindsym Shift+Print exec --no-startup-id "xfce4-screenshooter -f"

# Workspace Bindings
bindsym $mod+Home       workspace $ws1
bindsym $mod+Shift+Home     move container to workspace $ws1
bindsym $mod+End        workspace $ws10
bindsym $mod+Shift+End      move container to workspace $ws10
bindsym $mod+Prior      workspace prev
bindsym $mod+Shift+Prior    move container to workspace prev
bindsym $mod+Next       workspace next
bindsym $mod+Shift+Next     move container to workspace next
bindsym $mod+Tab        workspace back_and_forth
bindsym $mod+XF86Back       workspace prev
#bindsym $mod+Shift+XF86Back
bindsym $mod+XF86Forward    workspace next
#bindsym $mod+Shift+XF86Forward
bindsym $mod+semicolon      workspace next
bindsym $mod+apostrophe     split horizontal ;; exec $term
bindsym $mod+slash      split vertical ;; exec $term
bindsym $mod+Shift+slash    kill
bindsym $mod+backslash      workspace back_and_forth

# Startup applications

exec_always --no-startup-id setxkbmap -layout us -variant altgr-intl -option caps:escape
exec --no-startup-id steam-native -fulldesktopres
exec --no-startup-id clipit
exec --no-startup-id pasystray
# exec --no-startup-id polybar i3bar
exec --no-startup-id $HOME/.config/polybar/launch.sh
exec --no-startup-id gamemoded -d
exec --no-startup-id killall compton; compton -b
exec --no-startup-id nm-applet
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec_always --no-startup-id killall dunst; dunst -config ~/.config/dunst/config
exec_always --no-startup-id wal -i ~/Pictures/wallsnieuw
# exec_always --no-startup-id feh --bg-scale ~/Pictures/wallsnieuw/google.png; wal --theme base16-google -g -o wal_steam
exec_always --no-startup-id xrdb ~/.Xresources
exec --no-startup-id unclutter
exec --no-startup-id redshift -l 52:23 -t 6500:3200
exec --no-startup-id syncthing-gtk -m
#exec --no-startup-id blueberry

# For mpc
set $music $term -e ncmpcpp
set $pause --no-startup-id mpc toggle
set $dec --no-startup-id mpc volume -5
set $inc --no-startup-id mpc volume +5
set $trupause --no-startup-id mpc pause
set $next --no-startup-id mpc next
set $prev --no-startup-id mpc prev
set $lilfor --no-startup-id mpc seek +10
set $bigfor --no-startup-id mpc seek +120
set $lilbak --no-startup-id mpc seek -10
set $bigbak --no-startup-id mpc seek -120
set $beg --no-startup-id mpc seek 0%

bindsym $mod+p exec $pause
bindsym $mod+Shift+m exec $mute
bindsym $mod+plus exec $inc
bindsym $mod+Shift+plus exec $biginc
bindsym $mod+minus exec $dec
bindsym $mod+Shift+minus exec $bigdec
bindsym $mod+less exec $prev
bindsym $mod+Shift+less exec $beg
bindsym $mod+greater exec $next
bindsym $mod+Shift+greater exec $next
bindsym $mod+bracketleft exec $lilbak
bindsym $mod+Shift+bracketleft exec $bigbak
bindsym $mod+Shift+bracketright exec $bigfor
bindsym $mod+bracketright exec $lilfor
