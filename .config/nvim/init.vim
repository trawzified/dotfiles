set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc
" Vim-Plug Plugins
call plug#begin()

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-commentary'
Plug 'sheerun/vim-polyglot'
Plug 'matthewbdaly/vim-filetype-settings'
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'dylanaraps/wal.vim'
Plug 'junegunn/vim-plug'
Plug 'junegunn/goyo.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'ap/vim-css-color'
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'majutsushi/tagbar'
Plug 'Chiel92/vim-autoformat'

" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'SirVer/ultisnips'
" Plug 'Shougo/neco-syntax'

" Plug 'zchee/deoplete-jedi'
" Plug 'fishbullet/deoplete-ruby'
" Plug 'Shougo/deoplete-clangx'
Plug 'mzlogin/vim-markdown-toc'
Plug 'lervag/vimtex'
Plug 'lvht/phpcd.vim'
Plug 'eagletmt/neco-ghc'
Plug 'OmniSharp/omnisharp-vim'
Plug 'tpope/vim-dispatch'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': ':CocInstall coc-calc coc-gitignore coc-highlight coc-terminal coc-git coc-json coc-lists coc-marketplace coc-neosnippet coc-sql coc-ultisnips coc-snippets coc-syntax coc-xml coc-python coc-solargraph coc-ccls coc-java coc-phpls coc-tsserver coc-css coc-vimlsp coc-vimtex coc-omnisharp coc-html coc-cmake coc-clangd'}
Plug 'autozimu/LanguageClient-neovim', {
			\ 'branch': 'next',
			\ 'do': 'bash install.sh',
			\ }

call plug#end()

""" Plugins Requirements and settings

" Deoplete
" let g:deoplete#enable_at_startup = 1
" Remap completion to tab
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" Vim Airline Theme
let g:airline_theme = 'wal'
let g:airline_powerline_fonts = 1
let g:webdevicons_enable_ctrlp = 1
let g:webdevicons_enable_nerdtree = 1
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1
let g:WebDevIconsNerdTreeAfterGlyphPadding = '  '
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" goyo.vim - distraction-free writing in vim
xmap gy :Goyo \| set linebreak<CR>
nmap gy :Goyo \| set linebreak<CR>

" Language Client
set hidden
let g:LanguageClient_serverCommands = {
			\ 'rust': ['rustup', 'run', 'stable', 'rls'],
			\ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
			\ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
			\ 'python': ['/usr/local/bin/pyls'],
			\ 'cpp': ['clangd'],
			\ }

augroup filetype_rust
	autocmd!
	autocmd BufReadPost *.rs setlocal filetype=rust
augroup END

" Deoplete + LanguageClient
" call deoplete#custom#source('LanguageClient',
"             \ 'min_pattern_length',
"             \ 2)

" omnisharp
let g:OmniSharp_server_use_mono = 1 " Use mono system library
let g:OmniSharp_selector_ui = 'fzf'

" Vim Live Latex Preview
let g:livepreview_previewer = 'zathura'

" ale

let b:ale_fixers = ['prettier', 'eslint']

let g:ale_set_balloons = 1

let g:ale_sign_error = ''
let g:ale_sign_warning = ''

" Vim Autoformatter
noremap <F3> :Autoformat<CR>

" Tagbar
noremap <F4> :TagbarToggle<CR>

" NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

let g:NERDTreeIndicatorMapCustom = {
			\ "Modified"  : "",
			\ "Staged"    : "+",
			\ "Untracked" : "",
			\ "Renamed"   : "➜",
			\ "Unmerged"  : "",
			\ "Deleted"   : "",
			\ "Dirty"     : "",
			\ "Clean"     : "",
			\ "Ignored"   : "﬒",
			\ "Unknown"   : ""
			\ }

" coc.nvim
set sessionoptions+=globals
" let g:WorkspaceFolders = '/usr/include/SDL/SDL.h'

" EXPERIMENTAL
" Plug 'neovim/nvim-lsp'
" Plug 'Shougo/deoplete-lsp'
"""
" General Settings
colorscheme wal
