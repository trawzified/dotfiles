#!/bin/bash
# Credit to addy-fe (addy-dclxvi) for this awesome script!
# I modified it a little bit so it works for my suckless terminal.

# Draw a rectangle using slop then read the geometry value
read -r X Y W H < <(slop -f "%x %y %w %h" -b 3 -t 0 -q)

# Depends on font width & height
(( W /= 9 ))
(( H /= 31 ))

# Create a variable to be used for URxvt flag option
g=${W}x${H}+${X}+${Y}

# Remove current st rules
bspc rule -r St

# Draw with floating rule
bspc rule -a St -o state=floating follow=on focus=on
st -g $g &

# Restore the rule
bspc rule -a St desktop='^1' follow=on focus=on

