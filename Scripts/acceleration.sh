#!/bin/bash

sleep 5

#Disable Acceleration

xinput set-prop 11 'Device Accel Profile' -1

xinput set-prop 11 'Device Accel Velocity Scaling' 1 

xinput set-prop 12 'Device Accel Profile' -1

xinput set-prop 12 'Device Accel Velocity Scaling' 1 

xset m 0 0
