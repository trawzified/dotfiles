#!/usr/bin/env python3
import os
import sys
if(len(sys.argv)) > 1:

    if sys.argv[1] == "solarized":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-dge3pj.png')
        os.system('wal --theme solarized')

    elif sys.argv[1] == "gruvbox":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-6k59xl.png')
        os.system('wal --theme base16-gruvbox-hard')

    elif sys.argv[1] == "groot":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-96re38.jpg')

    elif sys.argv[1] == "yellow":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-132oe3.jpg')

    elif sys.argv[1] == "car":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-dg222g.png')

    elif sys.argv[1] == "cyberpunk":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-r266vw.jpg')

    elif sys.argv[1] == "scifi":
        os.system('wal -i ~/Pictures/wallsnieuw/wallhaven-73e9lv.jpg')

else:
    os.system('wal -i ~/Pictures/wallsnieuw')
