#!/bin/bash
# Copy important dotfiles in git repository.
cp $HOME/Scripts $HOME/Dotfiles/ -r

cp $HOME/.ncmpcpp $HOME/Dotfiles/ -r
cp $HOME/.xinitrc $HOME/Dotfiles/ -r
cp $HOME/.Xresources $HOME/Dotfiles/ -r
cp $HOME/.zshrc $HOME/Dotfiles/ -r
cp $HOME/.nvidia-xinitrc $HOME/Dotfiles/ -r
cp $HOME/.vimrc $HOME/Dotfiles/ -r
cp $HOME/.zprofile $HOME/Dotfiles/ -r

cp $HOME/.config/bspwm/ $HOME/Dotfiles/.config/bspwm/ -r
cp $HOME/.config/cava $HOME/Dotfiles/.config/ -r
cp $HOME/.config/cmus $HOME/Dotfiles/.config/ -r
cp $HOME/.config/clipit $HOME/Dotfiles/.config/ -r
cp $HOME/.config/dunst $HOME/Dotfiles/.config/ -r
cp $HOME/.config/i3 $HOME/Dotfiles/.config/ -r
cp $HOME/.config/mpd $HOME/Dotfiles/.config/ -r
cp $HOME/.config/neofetch $HOME/Dotfiles/.config/ -r
cp $HOME/.config/nvim $HOME/Dotfiles/.config/ -r
cp $HOME/.config/polybar $HOME/Dotfiles/.config/ -r
cp $HOME/.config/ranger $HOME/Dotfiles/.config/ -r
cp $HOME/.config/rofi $HOME/Dotfiles/.config/ -r
cp $HOME/.config/sxhkd $HOME/Dotfiles/.config/ -r
cp $HOME/.config/compton.conf $HOME/Dotfiles/.config/ -r

