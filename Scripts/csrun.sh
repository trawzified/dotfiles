#!/bin/bash

echo "Enter the number of the C# file you want to execute:"

PS3="Your choice: "
QUIT="Quit now .cs"
touch "$QUIT"

select FILENAME in *.cs; do
  case $FILENAME in
        "$QUIT")
          echo "Exiting."
          break
          ;;
        *)
          echo "Now compiling $FILENAME ($REPLY)"
          mcs "$FILENAME"
	  echo "Now launching $FILENAME ($REPLY)..."
	  echo "-----------------------------------" && sleep 1
	  FILENAME=${FILENAME%"cs"}
	  FILENAME="${FILENAME}exe"
	  mono "$FILENAME"
	  break
	  ;;
  esac
done
rm "$QUIT"
rm "$FILENAME"
