# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

#PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

PATH=$HOME/Scripts/Pywalfox/daemon/pywalfox.py:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

# ZSH_THEME="terminalparty"
ZSH_THEME="agnoster"
#ZSH_THEME="zhann"
# ZSH_THEME="bira"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

# Zsh Syntax Highlighting installation:
# git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Zsh Auto Suggestions installation:
# git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

plugins=(git zsh-syntax-highlighting zsh-autosuggestions extract colored-man-pages copyfile)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

export LANG=en_US.UTF-8
export EDITOR='nvim'
export TERM=xterm-256color

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
	if [[ ${KEYMAP} == vicmd ]] ||
		[[ $1 = 'block' ]]; then
			echo -ne '\e[1 q'
		elif [[ ${KEYMAP} == main ]] ||
			[[ ${KEYMAP} == viins ]] ||
			[[ ${KEYMAP} = '' ]] ||
			[ $1 = 'beam' ]]; then
					echo -ne '\e[5 q'
	fi
}
zle -N zle-keymap-select
zle-line-init() {
zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
	tmp="$(mktemp)"
	lf -last-dir-path="$tmp" "$@"
	if [ -f "$tmp" ]; then
		dir="$(cat "$tmp")"
		rm -f "$tmp"
		[ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
	fi
}
bindkey -s '^o' 'lfcd\n'

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

alias ls='lsd --group-dirs=first --date=relative'
alias l='lsd -l --group-dirs=first --date=relative'
alias la='lsd -a --group-dirs=first --date=relative'
alias lt='lsd --tree'
alias up='yay'
alias sudo='sudo -p "You have no power here, %u the %h!"'
alias it='yay -S'
alias un='sudo pacman -Rs'
alias sr="yay -s"
alias mu="ncmpcpp"
alias music="ncmpcpp"
alias bk="cd .."
alias back="cd .."
alias count="ls -1 | wc -l"
alias sabnzbd="python /opt/sabnzbd/SABnzbd.py"
alias wgpu="glxinfo | grep \"server glx vendor string\" && glxinfo | grep \"OpenGL renderer\" && vulkaninfo | grep -i GPU"
alias progs="(pacman -Qet && pacman -Qm) | sort -u"
alias theme="~/Scripts/themes.py"

# Credit to Luke Smith (lukesmithxyz) for some of these.
alias v="nvim -p"
alias vim="nvim -p"
alias ka="killall"
alias sv="sudo nvim -p"
alias g="git"
alias gc="git commit -m"
alias gp="git push"
alias crep="grep --color=always"
alias sdn="shutdown now"
alias yt="youtube-dl -ic"
alias yta="youtube-dl -xic --audio-format mp3"
alias starwars="telnet towel.blinkenlights.nl"
alias nf="clear && neofetch"
alias newnet="sudo systemctl restart NetworkManager"
alias tr="transmission-remote"
alias bw="wal -i ~/Pictures/wallsnieuw"
alias cl="clear"

# Music
alias pause="mpc toggle"
alias next="mpc next"
alias prev="mpc prev"
alias trupause="mpc pause"
alias beg="mpc seek 0%"
alias lilbak="mpc seek -10"
alias lilfor="mpc seek +10"
alias bigbak="mpc seek -120"
alias bigfor="mpc seek +120"

# Directory/File Shortcuts:
alias h="cd ~ && ls -a"
alias d="cd ~/Downloads && ls -a"
alias D="cd ~/Documents && ls -a"
alias p="cd ~/Pictures && ls -a"
# alias v="cd ~/Videos && ls -a"
alias V="cd ~/Videos && ls -a"
alias m="cd ~/Music && ls -a"
alias r="cd / && ls -a"
alias cf="cd ~/.config && ls -a"
alias S="cd ~/Sync && ls -a"
alias W="cd ~/Documents/Websites && ls -a"

alias cfz="nvim ~/.zshrc"
alias cfr="nvim ~/.config/ranger/rc.conf.base"
alias cfi="nvim ~/.config/i3/config"
alias cfb="nvim ~/.config/bspwm/bspwmrc"
alias cfp="nvim ~/.config/polybar/config.ini"
alias cfx="nvim ~/.Xresources"
alias cfs="nvim ~/.config/sxhkd/sxhkdrc"
alias cfc="nvim ~/.config/picom.conf"
alias cfv="nvim ~/.config/nvim/init.vim"

# Replacements
#alias sudo='sudo -p "You have no power here, " + ${whoami} + "!"'
#alias sudo="sudo -p 'You have no power here, %u'"
#alias sudo="sudo "
alias sudo='sudo -p "You have no power here, %u the %h!
" '
alias e="exit"
alias c="clear"
alias mkdir="mkdir -pv"
alias cp="rsync --info=progress2 -ip"
alias wal="wal -o '$HOME/Scripts/Pywalfox/daemon/pywalfox.py update'"
alias pywalfox="$HOME/Scripts/Pywalfox/daemon/pywalfox.py"
alias ncdu="ncdu --color dark"

# When you're not using lsd for some reason, use this.
# alias ls='ls --color=auto --group-directories-first'
